package com.televisa.veotest.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.televisa.veotest.R;
import com.televisa.veotest.models.AssetDetails;
import com.televisa.veotest.models.AssetDetailsAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AssetDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class AssetDetailsFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String ARG_PARAM1 = "param1";

    private ArrayList<AssetDetails> mParam1;

    private OnFragmentInteractionListener mListener;

    public AssetDetailsFragment() {
        // Required empty public constructor
    }

    public static AssetDetailsFragment newInstance(ArrayList<AssetDetails> param1) {
        AssetDetailsFragment fragment = new AssetDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = (ArrayList<AssetDetails>) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_asset_details, container, false);

        ListView listViewAssetDetails = (ListView) view.findViewById(R.id.listViewAssetDetails);
        listViewAssetDetails.setAdapter(new AssetDetailsAdapter(getContext(), mParam1));
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
