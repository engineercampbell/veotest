package com.televisa.veotest.models;

import java.io.Serializable;

public class AssetDetails implements Serializable {

    String title;
    String synopsis;

    public AssetDetails(String _title, String _synopsis) {
        title = _title;
        synopsis = _synopsis;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String _title) {
        title = _title;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String _synopsis) {
        synopsis = _synopsis;
    }
}
