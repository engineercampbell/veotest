package com.televisa.veotest.models;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.televisa.veotest.R;

import java.util.List;

/**
 * Created by Human on 1/27/2016.
 */
public class AssetDetailsAdapter extends BaseAdapter {
    Context context;
    List<AssetDetails> assetDetailsList;

    public AssetDetailsAdapter(Context context, List<AssetDetails> assetDetailsList) {
        this.context = context;
        this.assetDetailsList = assetDetailsList;
    }

    @Override
    public int getCount() {
        return assetDetailsList.size();
    }

    @Override
    public Object getItem(int position) {
        return assetDetailsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return assetDetailsList.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            convertView = mInflater.inflate(R.layout.row_asset_details, null);

        }
        AssetDetails currentAsset = assetDetailsList.get(position);

        // Add received info to UI
        TextView textViewTitle = (TextView) convertView.findViewById(R.id.textViewAssetTitle);
        textViewTitle.setText(currentAsset.getTitle());

        TextView textViewSynopsis = (TextView) convertView.findViewById(R.id.textViewAssetSynopsis);
        textViewSynopsis.setText(currentAsset.getSynopsis());
        return convertView;
    }
}
