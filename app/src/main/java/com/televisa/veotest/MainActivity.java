package com.televisa.veotest;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.televisa.veotest.fragments.AssetDetailsFragment;
import com.televisa.veotest.fragments.MainFragment;
import com.televisa.veotest.fragments.TopperFragment;
import com.televisa.veotest.models.AssetDetails;
import com.televisa.veotest.models.Source;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        final ArrayList<AssetDetails> assetDetails = new ArrayList<>();

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AssetDetailsFragment assetDetailsFragment = new AssetDetailsFragment();
                Bundle args = new Bundle();
                args.putSerializable(AssetDetailsFragment.ARG_PARAM1, assetDetails);
                assetDetailsFragment.setArguments(args);
                getSupportFragmentManager().beginTransaction()
                        .setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fragment_container, assetDetailsFragment)
                        .commit();
                fab.setVisibility(View.INVISIBLE);
            }
        });

        Source source = null;
        try {
            Gson gson = new Gson();
            source = gson.fromJson(loadJSONFromAsset(), Source.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

        if (savedInstanceState == null) {
            MainFragment mainFragment = new MainFragment();
            Bundle args = new Bundle();
            assert source != null;
            args.putString(MainFragment.ARG_PARAM1, source.getData().getPictures().get(0).getVersions().get(0).getLink());
            mainFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .add(R.id.fragment_container, mainFragment).commit();
        }
        assert source != null;
        setTopperFragmentUI(source.getData().getTitle(), source.getData().getSynopsis(), source.getData().getAirDate());

        for (int i = 0; i < source.getData().getSeasons().get(0).getEpisodeCount(); i++) {
            assetDetails.add(new AssetDetails(source.getData().getSeasons().get(0).getEpisodes().get(i).getTitle()
                    , source.getData().getSeasons().get(0).getEpisodes().get(i).getSynopsis()));
        }


    }

    public String loadJSONFromAsset() {
        String json;
        try {

            InputStream is = this.getResources().openRawResource(R.raw.testasset);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void setTopperFragmentUI(String showtitle, String showsynopsis, String showairdate) {
        TopperFragment topperFragment = (TopperFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_topper);
        topperFragment.textViewShowTitle.setText(showtitle);
        topperFragment.textViewShowSynopsis.setText(showsynopsis);
        topperFragment.textViewShowAirDate.setText(showairdate);
    }


}
